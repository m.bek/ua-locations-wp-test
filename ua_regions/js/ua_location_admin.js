jQuery(document).on("change", ".location_type", function(){
    jQuery("#parent-region, #parent-city").hide();
    if (jQuery(this).val() == 2) {
        jQuery("#parent-region").show();
    } else if (jQuery(this).val() == 3) {
        jQuery("#parent-region").show();
        jQuery("#parent-city").show();
    }
});

jQuery(document).on("change", ".region-select", function(){
    console.log(jQuery(this).val());
    var region = jQuery(this).val();

    jQuery.ajax({
        url: "/wp-admin/admin-ajax.php?action=get_cities_list",
        data: {region_id: region},
        type: "GET",
        dataType: "html",
        success: function(html) {
            jQuery(".city-select").html(html);
        }
    });

});

function validateLocationForm(form)
{
    jQuery(form).find(".error").remove();

    var lType = jQuery(form).find("select[name='type']").val();
    var valid = true;

    if (lType == 0) {
        jQuery(form).find("select[name='type']").after('<p class="error">Please Choose Location Type</p>');
        valid = false;
    } else if (lType == 2 && jQuery(".region-select").val() == 0) {
        jQuery(form).find("select[name='region_id']").after('<p class="error">Please Choose Region</p>');
        valid = false;
    } else if (lType == 3 && (jQuery(".region-select").val() == 0 || jQuery(".city-select").val() == 0) ) {
        if (jQuery(".region-select").val() == 0) {
            jQuery(".region-select").after('<p class="error">Please Choose Region</p>');
        }
        if (jQuery(".city-select").val() == 0) {
            jQuery(".city-select").after('<p class="error">Please Choose City</p>');
        }
        valid = false;
    }

    return valid;
}