<?php
/*
Plugin Name: UA Regions
Description: Adding/Editing regions.
Version: 1.0
Author: Maksym Bek
*/
global $wpdb;
$UAL_plugin_url = WP_PLUGIN_URL."/".str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
define("UAL_PLUGIN_PATH", $UAL_plugin_url);

define("UA_LOCATION_TABLE", $wpdb->prefix . "regions");
define("UA_LOCATION_URL", get_bloginfo("siteurl").'/wp-admin/admin.php?page=locations');

register_activation_hook( __FILE__, 'install_regions' );
function install_regions()
{
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE " . UA_LOCATION_TABLE . "(
                `id` INT(9) NOT NULL AUTO_INCREMENT,
                `title` VARCHAR(128) NOT NULL,
                `status` TINYINT(1) NOT NULL DEFAULT 1,
                `type` TINYINT(1) NOT NULL DEFAULT 0,
                `region_id` INT(9) NOT NULL DEFAULT 0,
                `city_id` INT(9) NOT NULL DEFAULT 0
                PRIMARY KEY (`id`)
            )" . $charset_collate . ";";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

add_action('admin_menu', 'create_menu');
// code for all menus
function create_menu()
{
    add_menu_page( 'UA Locations', 'Locations', 10, 'locations', 'locations_function','',7);
    add_submenu_page( 'ua_locations', 'Regions', 'Regions', 10, 'add_region', 'add_region_function' );
}

function ual_admin_scripts()
{
    wp_register_script('ua_location_admin', UAL_PLUGIN_PATH.'js/ua_location_admin.js', array('jquery'));
    wp_enqueue_script("ua_location_admin");
}
add_action('admin_print_scripts', 'ual_admin_scripts');

function ual_admin_styles()
{
    wp_enqueue_style('ual-styles', UAL_PLUGIN_PATH.'css/ual-styles-admin.css');

}
add_action('admin_print_styles', 'ual_admin_styles');


function locations_function()
{
    $action = $_GET["action"];

    if (empty($action)) {
        locations_list();
    } elseif ($action == 'add') {
        add_location();
    } elseif ($action == 'edit') {
        edit_location();
    } elseif ($action == 'delete') {
        delete_location();
    } elseif ($action == 'save') {
        save_location_form();
    }

}

function locations_list()
{
    global $wpdb;

    $locations = get_sorted_locations();
?>
<div class="wrap">
    <h2>UA Locations &nbsp;<a href="<?= UA_LOCATION_URL . "&action=add" ?>" class="page-title-action">Add Location</a></h2>
    <hr />
    <form action="" name="frmmilestone" method="post">
        <table cellpadding="2" cellspacing="1"  class="wp-list-table widefat fixed" border="0" width="100%">
            <thead>
            <tr>
                <th width="30">No.</th>
                <th width="100">Region/City/Village</th>
                <th width="100">Type</th>
                <th width="100">Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th width="30">No.</th>
                <th>Region/City/Village</th>
                <th width="100">Type</th>
                <th width="100">Action</th>
            </tr>
            </tfoot>
            <tbody>
            <?php
            if (count($locations)) {
                $i = 1;
                _location_table($locations, $i);
            } else { ?>
                <tr class="alternate">
                    <td colspan="4" align="center">No Locations Found</td>
                </tr>
                <?php
            } ?>
            </tbody>
        </table>
    </form>
</div>
<?php
}

function add_location()
{
    $data = array();

    get_location_form($data);
}

function edit_location()
{
    global $wpdb;
    $data = array();
    if (isset($_GET['id'])) {
        $data = $wpdb->get_row("SELECT * FROM " . UA_LOCATION_TABLE . " WHERE id = " . (int) $_GET['id'], ARRAY_A);
    }

    get_location_form($data);
}

function get_location_form($data)
{
    global $wpdb;
    $regions = $wpdb->get_results( "SELECT id, title FROM " . UA_LOCATION_TABLE . " WHERE type = 1" );

    $cities = array();
    if (count($data) && isset($data['region_id'])) {
        $cities = $wpdb->get_results( "SELECT id, title FROM " . UA_LOCATION_TABLE . " WHERE type = 2 AND region_id = " . (int) $data['region_id'] );
    }

    $action = UA_LOCATION_URL . "&action=save";
    ?>
    <div class="wrap">
        <h2><?php if (count($data)) {echo "Edit";} else {echo "Add";} ?> Location &nbsp;
            <hr />
            <form method="post" action="<?= $action ?>" onsubmit="return validateLocationForm(this);" id="form-location" name="form-location">
                <div class="postbox">
                    <div class="inside">
                        <table class="form-table">
                            <tbody>
                            <tr valign="top">
                                <td width="25%" align="left">
                                    <strong>Enter Location:</strong>
                                </td>
                                <td align="left">
                                    <input placeholder="Enter Location" class="" name="title" type="text" value="<?php echo isset($data['title']) ? $data['title'] : ''; ?>">
                                </td>
                            </tr>
                            <tr valign="top">
                                <td width="25%" align="left">
                                    <strong>Location Type:</strong>
                                </td>
                                <td align="left">
                                    <select name="type" class="location_type">
                                        <option value="0">-- Choose Location Type --</option>
                                        <option value="1"<?= (isset($data['type']) && $data['type'] == 1) ? ' selected="selected"' : ''; ?>>Region</option>
                                        <option value="2"<?= (isset($data['type']) && $data['type'] == 2) ? ' selected="selected"' : ''; ?>>City/Township</option>
                                        <option value="3"<?= (isset($data['type']) && $data['type'] == 3) ? ' selected="selected"' : ''; ?>>Village</option>
                                    </select>
                                </td>
                            </tr>
                            <tr valign="top" id="parent-region" class="<?= isset($data['type']) ? "visible" : "hidden"; ?>">
                                <td width="25%" align="left">
                                    <strong>Choose Region:</strong>
                                </td>
                                <td align="left">
                                    <select name="region_id" class="region-select">
                                        <option value="0">-- Choose Region --</option>
                                        <?php
                                        if (count($regions) > 0) {
                                            foreach ($regions as $region) { ?>
                                                <option value="<?= $region->id ?>"
                                                    <?= (isset($data['region_id']) && $data['region_id'] == $region->id) ? ' selected="selected"' : ""; ?>>
                                                    <?= $region->title; ?>
                                                </option>
                                            <?php }
                                        } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr valign="top" id="parent-city" class="<?= (isset($data['type']) && $data['type'] == 3) ? "visible" : "hidden"; ?>">
                                <td width="25%" align="left">
                                    <strong>Choose City:</strong>
                                </td>
                                <td align="left">
                                    <select name="city_id" class="city-select">
                                        <option value="0">-- Choose City --</option>
                                        <?php
                                        if (count($cities) > 0) {
                                        foreach ($cities as $city) { ?>
                                            <option value="<?= $city->id ?>"
                                                <?= (isset($data['city_id']) && $data['city_id'] == $city->id) ? ' selected="selected"' : ""; ?>>
                                                <?= $city->title; ?>
                                            </option>
                                        <?php }
                                        } ?>

                                    </select>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td width="25%" align="left">
                                    <strong>Status:</strong>
                                </td>
                                <td align="left">
                                    <select name="status">
                                        <option value="1">Enabled</option>
                                        <option value="0">Disabled</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="submit">
                    <input type="hidden" name="id" value="<?php echo isset($data['id']) ? $data['id'] : 0 ;?>" />
                    <input type="submit" class="button-primary" name="edit_location" value="Save »">
                </div>
            </form>
    </div>
<?php
}

function save_location_form()
{
    global $wpdb;

    $data = array(
        'title' => $_POST['title'],
        'status' => (int) $_POST['status'],
        'type' => (int) $_POST['type']
    );

    switch ($_POST['type']) {
        case 1 :
            $data['region_id'] = 0;
            $data['city_id'] = 0;
            break;
        case 2 :
            $data['region_id'] = (int) $_POST['region_id'];
            $data['city_id'] = 0;
            break;
        case 3 :
            $data['region_id'] = (int) $_POST['region_id'];
            $data['city_id'] = (int) $_POST['city_id'];
            break;
    }

    if ($_POST['id'] > 0) {
        $wpdb->update(UA_LOCATION_TABLE, $data, array("id" => (int) $_POST['id']) );
    } else {
        $wpdb->insert(UA_LOCATION_TABLE, $data);
    }

    redirect(UA_LOCATION_URL);
}

function _location_table($data, &$i)
{
    foreach ($data as $location) {
        $prefix = "";
        $type = "Region";
        if ($location['type'] == 2) {
            $prefix = " - ";
            $type = "City/Township";
        } elseif ($location['type'] == 3) {
            $prefix = " -- ";
            $type = "Village";
        }
        ?>
        <tr class="alternate">
            <th><?php echo $i; ?></th>
            <th>
                <?php
                echo $prefix .$location['title'];
                ?>
            </th>
            <th><?= $type ?></th>
            <th>
                <a href="<?php echo UA_LOCATION_URL; ?>&action=delete&id=<?php echo $location['id']; ?>"
                   onclick="return confirm('Are you sure?');">
                    Delete
                </a>
                |
                <a href="<?php echo UA_LOCATION_URL; ?>&action=edit&id=<?php echo $location['id']; ?>">Edit</a>
            </th>
        </tr>
        <?php
        $i++;
        if (isset($location['cities']) && !empty($location['cities'])) {
            _location_table($location['cities'], $i);
        }
        if (isset($location['villages']) && !empty($location['villages'])) {
            _location_table($location['villages'], $i);
        }
    }
}

function get_cities_list()
{
    global $wpdb;

    $options = '<option value="0">-- Choose City --</option>';
    if (isset($_GET['region_id'])) {
        $results = $wpdb->get_results("SELECT id, title FROM " . UA_LOCATION_TABLE . " WHERE `type` = 2 AND `region_id` = " . (int) $_GET['region_id'] . " ORDER BY title");

        if (count($results)) {
            foreach ($results as $result) {
                $options .= '<option value="' . $result->id . '">' . $result->title . '</option>';
            }
        }
    }

    echo $options;
    exit();
}

function get_sorted_locations( $activeOnly = false)
{
    global $wpdb;

    $activeQuery = '';
    if ($activeOnly === true)
        $activeQuery = " WHERE `status` = 1 ";

    $results = $wpdb->get_results("SELECT * FROM " . UA_LOCATION_TABLE . $activeQuery . " ORDER BY region_id, title");

    $locations = array();
    if (count($results)) {
        foreach ($results as $item) {
            if ($item->type == 1) {
                $locations[$item->id] = array(
                    'title' => $item->title,
                    'id' => $item->id,
                    'type' => $item->type
                );
            } elseif ($item->type == 2 && $item->region_id > 0) {
                if (!array_key_exists($item->region_id, $locations)) {
                    $locations[$item->region_id] = array(
                        'id' => $item->region_id,
                        'type' => 1,
                        'cities' => array(
                            $item->id => array(
                                'title' => $item->title,
                                'id' => $item->id,
                                'type' => 2
                            )
                        )
                    );
                } else {
                    $locations[$item->region_id]['cities'][$item->id] = array(
                        'title' => $item->title,
                        'id' => $item->id,
                        'type' => 2
                    );
                }
            } elseif ($item->type == 2 && $item->region_id == 0) {
                $locations[$item->id] = array(
                    'title' => $item->title,
                    'id' => $item->id,
                    'type' => $item->type
                );
            } elseif ($item->type == 3) {
                $locations[$item->region_id]['cities'][$item->city_id]['villages'][$item->id] = array(
                    'title' => $item->title,
                    'id' => $item->id,
                    'type' => $item->type
                );
            }
        }
    }

    return $locations;
}

add_action( 'wp_ajax_get_cities_list', 'get_cities_list' );
add_action( 'wp_ajax_nopriv_get_cities_list', 'get_cities_list' );

function delete_location()
{
    if (isset($_GET['id']) && (int) $_GET['id'] > 0) {
        global $wpdb;
        $wpdb->query("DELETE FROM " . UA_LOCATION_TABLE . " WHERE id = " . (int) $_GET['id']);
    }

    redirect(UA_LOCATION_URL);
}

function redirect($url){
    echo "<script language='javascript'>window.location.href='".$url."'</script>";
    exit;
}

add_filter( 'wp_nav_menu_items', 'ua_locations_menu_items', 10, 2 );
function ua_locations_menu_items ( $items, $args ) {

    $locations = get_sorted_locations(true);

    if (count($locations) && $args->theme_location == 'primary') {
        $items .= _get_menu_items($locations);
    }
    return $items;
}

function _get_menu_items($locations)
{
    $html = '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" aria-haspopup="true">
            <a href="#" class="">Regions</a><button class="dropdown-toggle" aria-expanded="false"><span class="screen-reader-text">expand child menu</span></button>
            <ul class="sub_menu">
        ';
    foreach ($locations as $item) {
        $class = '';
        if (isset($item['cities']) && count($item['cities'])) {
            $class = ' menu-item-has-children';
        }
        $html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page' . $class . '" aria-haspopup="true">
            <a href="#" class="">' . $item['title'] . '</a>';
        if (isset($item['cities']) && count($item['cities'])) {
            $html .= '<button class="dropdown-toggle" aria-expanded="false"><span class="screen-reader-text">expand child menu</span></button>
                <ul class="sub_menu">';

            foreach ($item['cities'] as $city) {
                $cityClass = '';
                if (isset($city['villages']) && count($city['villages'])) {
                    $cityClass = ' menu-item-has-children';
                }
                $html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page' . $cityClass . '">
                    <a href="#">' . $city['title'] . '</a>';
                if (isset($city['villages']) && count($city['villages'])) {
                    $html .= '<button class="dropdown-toggle" aria-expanded="false"><span class="screen-reader-text">expand child menu</span></button>
                        <ul class="sub_menu">';
                    foreach ($city['villages'] as $village) {
                        $html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="#">' . $village['title'] . '</a></li>';
                    }
                    $html .= '</ul>';
                }
                $html .= '</li>';
            }

            $html .= '</ul>';
        }
        $html .= '</li>';
    }

    $html .= '</ul></li>';

    return $html;
}